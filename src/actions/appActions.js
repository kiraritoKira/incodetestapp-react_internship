export const SET_INPUT_TEXT = "SET_INPUT_TEXT"
export const CHOOSE_CLIENT = "CHOOSE_CLIENT"
export const GET_CLIENTS_REQUEST = 'GET_CLIENTS_REQUEST'
export const GET_CLIENTS_SUCCESS = 'GET_CLIENTS_SUCCESS'

import db from '../db/db'

export const setInputText = (text) => {
    return {
      type: SET_INPUT_TEXT,
      payload: text
    }
}

export const chooseClient = (client) => {
    return {
        type : CHOOSE_CLIENT,
        payload: client
    }
}

export const getClients = () => {
  return dispatch => {
    
    dispatch({
      type: GET_CLIENTS_REQUEST
    })

    setTimeout(() => {
      dispatch({
        type: GET_CLIENTS_SUCCESS,
        payload: db.getItems()
      })
    }, 1000)
  }
}