import React, { Component } from "react"
import ClientForm from './ClientForm'
import ClientFullInfo from './ClientFullInfo'


class App extends Component {
    render () {
        return (
            <div className="app">
                <ClientForm/>
                <ClientFullInfo/>
            </div>
        )
    }
}

export default App