import React from "react"

const SearchClientInput = ({onChangeHandler}) => {
    
    const [
        inputPlaceHolder,
        inputDefaultValue
    ] = [
        "Search for client:", 
        ""
    ]
    
    return(
        <div className="searchComponent">
            <input type="text" 
                   placeholder={inputPlaceHolder} 
                   dafaultvalue={inputDefaultValue} 
                   onChange={(e) => onChangeHandler(e.target.value)} />
        </div>
    )
}

export default SearchClientInput