import React from "react"
import { connect } from 'react-redux'

const NoOneIsChoosen = () => {
    return (    
        <div className="onPageReady">
            <div>
                Choose client to get more info.            
            </div>
        </div>
    )
}

const ClientIsChoosen = ({clientInfo}) => {
    return (
        <div className = 'clientInfo'>
            <div className="photoCard">
                <img src={`${clientInfo.general.avatar}`} alt=""/>
            </div>
            <div className="card">
                <h1>General</h1>
                <div>Name: {clientInfo.general.firstName}</div>
                <div>Lastname: {clientInfo.general.lastName}</div>
                <div>Job: {clientInfo.job.title}</div>
                <h1>Contact</h1>
                <div>Email: {clientInfo.contact.email}</div>
                <div>Phone: {clientInfo.contact.phone}</div>
                <h1>Address</h1>
                <div>City: {clientInfo.address.city}</div>
                <div>Street: {clientInfo.address.street}</div>
            </div>
        </div>
    )
}


class ClientFullInfo extends React.Component {
    render() {
        const {choosenClient} = this.props.app
        const choosenClientIsNotEmptyObject = choosenClient.general

        return choosenClientIsNotEmptyObject ? 
            <div className="clientFullInfoContainer">
                <ClientIsChoosen clientInfo = {choosenClient}/>
            </div> :
            <div className="clientFullInfoContainer">
                <NoOneIsChoosen/>
            </div>
            
    }  
}



const mapStateToProps = store => {
    return {
      app : store.app
    }
  }
  
 
export default connect(
      mapStateToProps
)(ClientFullInfo)

