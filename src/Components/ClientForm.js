import React from 'react'
import SearchClientInput from './SearchClientInput'
import ClientsList from './ClientsList'
import { connect } from 'react-redux'
import {setInputText, chooseClient, getClients} from '../actions/appActions'

class ClientForm extends React.Component {
    
    componentDidMount() {
        this.props.getClientsAction()
    }

    render() {
        const {app, setInputValueAction, chooseTheClickedClient} = this.props
        return (
            <div className="clientForm">
                <SearchClientInput onChangeHandler = {setInputValueAction}/>
                <ClientsList clientsArray = {app.clients} 
                             onItemClickHandler={chooseTheClickedClient} 
                             isLoading = {app.isLoading} 
                             choosenClientName = {app.formInputValue} 
                             clickedClient = {app.choosenClient}/>
            </div>
        )
    }
    
}


const mapDispatchToProps = dispatch => ({
    setInputValueAction: text => dispatch(setInputText(text)),
    chooseTheClickedClient : client => dispatch(chooseClient(client)),
    getClientsAction : () => dispatch(getClients())
})

const mapStateToProps = store => {
    return {
      app : store.app
    }
  }
  
 
export default connect(
      mapStateToProps,
      mapDispatchToProps
)(ClientForm)