import React from 'react'

const isClicked = (statement) => {
    return statement ? "clickedClient" : "" 
}

const Client = ({onItemClickHandler , client, clickedClient}) => {
    return (
        <div className={`clientItem ${isClicked(client === clickedClient)}`} 
             onClick = {() => onItemClickHandler(client)}>
            <div className="img">
                <img src={`${client.general.avatar}`} alt=""/>
            </div> 
            <div>{client.general.firstName}</div>
        </div>  
    ) 
}

const NoClientWasFound = () => {
    return (
        <div className="warning">
            {"Nothing was found by your request..."}
        </div>
    )
}

const DataIsLoading = () => {
    return (
        <div className="warning">
            {"Data is loading..."}
        </div>
    )
}

const filterClientsArray = (arr, choosenClientName) => {
    return arr.length > 0 ? 
            arr.filter((elem) => {
                return elem.general.firstName.toLowerCase().startsWith(choosenClientName) ||
                       elem.general.lastName.toLowerCase().startsWith(choosenClientName) }) : 
            []
}

const showIsLoadingCard = (isLoading) => {
    return isLoading ? <DataIsLoading/> : <NoClientWasFound/>
}

const getCashFromClientElement = (elem) => {
    return elem.general.firstName.toLowerCase() +
           elem.general.lastName.toLowerCase()
}

const ClientsList = ({clientsArray, choosenClientName, isLoading, onItemClickHandler, clickedClient}) => {
    const filteredClientsArray = filterClientsArray(clientsArray, choosenClientName) 
    return (
        <div className="clientList">
            {filteredClientsArray.length > 0 ? 
                filteredClientsArray.map((elem) => {
                    let key = getCashFromClientElement(elem)
                    return <Client key = {key} 
                                   onItemClickHandler = {onItemClickHandler} 
                                   clickedClient = {clickedClient} 
                                   client = {elem}/>
                }) : 
                showIsLoadingCard(isLoading)
            }
        </div>
    )
}

export default ClientsList