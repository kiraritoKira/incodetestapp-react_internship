import {SET_INPUT_TEXT, CHOOSE_CLIENT} from '../actions/appActions'
import { GET_CLIENTS_REQUEST, GET_CLIENTS_SUCCESS } from '../actions/appActions'


export const initialState = {
    clients : [],
    formInputValue: '',
    choosenClient : {},
    isLoading: false
}
  
export function appReducer(state = initialState, action) {
    switch (action.type) {
        case SET_INPUT_TEXT:
          return { ...state, formInputValue: action.payload }
        case CHOOSE_CLIENT: 
          return {... state, choosenClient: action.payload }
        case GET_CLIENTS_REQUEST:
          return { ...state, isLoading: true }
        case GET_CLIENTS_SUCCESS:
          return { ...state, clients: action.payload, isLoading: false }
    
        default:
          return state
    }
}